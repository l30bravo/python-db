# Demo Python3 + PostgreSQL

## Dependencias

Instalar biblioteca para conexión con postgres


```bash
pip install psycopg2
```


## Configuración

Editar el archivo database.ini

## Ejecución

* Demo 1 - Entrega la version del servidor de base de datos instalada

```bash
python db_test.py
```

* Demo 2 - Consulta una tabla de la base de datos

```bash
python db_test2.py
```


